export default {
    state: {
        chosen_model: ""
    },
    mutations: {
        change_model(state, value) {
            state.chosen_model = value;
        }
    },
    actions: {},
    getters: {
        get_chosen_model(state) {
            return state.chosen_model;
        }
    }
};
