<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        'https://tovilht.com/api/sales/application',
        'https://tovilht.com/api/allastar/posts',
        'https://tovilht.com/api/allastar/new_post'
    ];
}
