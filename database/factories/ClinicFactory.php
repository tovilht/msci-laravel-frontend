<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Clinic;
use Faker\Generator as Faker;

$factory->define(Clinic::class, function (Faker $faker) {
    // $user = App\User::latest()->first();

    if (App\Sale::latest()->first()) {
        $sale =  App\Sale::latest()->first()->id;
    } else {
        $sale = 1;
    }
    return [
        // 'user_id' => $user->id,
        'sale_id' => $sale,
        'name' => $faker->company,
        'district_id' => $faker->numberBetween($min = 1, $max = 19),
        'clinic_chinese_name' => $faker->name,
        'latitude' => $faker->latitude(22.2,22.38),
        'longitude' => $faker->longitude(114.12,114.2)
    ];
});
