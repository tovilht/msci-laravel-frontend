import axios from "axios";

export default {
    state: {
        authenticate: false,
        userData: {}
    },
    mutations: {
        change_auth(state, value) {
            state.authenticate = value;
        },
        change_user_data(state, value) {
            state.userData = value;
        }
    },
    actions: {
        check_logged_in() {
            return axios
                .get("/api/user")
                .then(response => {
                    this.commit("change_auth", true);
                    this.commit("change_user_data", response.data);
                    this.state.userData = response.data;
                })
                .catch(error => {
                    this.commit("change_auth", false);
                    this.commit("change_user_data", {});
                });
        }
    },
    getters: {
        get_user_data(state) {
            return state.userData;
        }
    }
};
