<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    static $count = 1;
    return [
        //
        'name' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'description' => $faker->sentence($nbWords = 15, $variableNbWords = true),
        'price' => $faker->numberBetween($min = 50, $max = 200),
        'clinic_price' => $faker->numberBetween($min = 1, $max = 50),
        'weight' => $faker->numberBetween($min = 500, $max = 2000),
        'ordering' => $count++
    ];
});
