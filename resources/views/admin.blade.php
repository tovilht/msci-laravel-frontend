<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

  <title>Admin</title>

  <!-- Quicksand Font -->
  <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@500;600;700&display=swap" rel="stylesheet">
  <link rel="apple-touch-icon" sizes="128x128" href="/images/avatars/edel_icon.jpg">
  <link rel="icon" sizes="192x192" href="/images/avatars/edel_icon.jpg">

</head>

<body>
  <noscript>
    <strong>We're sorry but <%= htmlWebpackPlugin.options.title %> doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
  </noscript>

  <div id="app"></div>
  <!-- built files will be auto injected -->

  <script src="{{asset('admin/main.js')}}"></script>
</body>

</html>