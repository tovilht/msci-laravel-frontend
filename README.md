<!-- TABLE OF CONTENTS -->

## Table of Contents

<details open="open">
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#folder-structure">Folder Structure</a></li>
    <li><a href="#code-explanation">Code Explanation</a>
    <ul>
        <li><a href="#file-conversion">FileConversion.py</a></li>
        <li><a href="#predict">Predict.py</a></li>
        <li><a href="#app">App.py</a></li>
      </ul>
    </li>
   
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

The project is built for the classifcation of various types of DNAs.

this Python backend will serve the following purposes:

1. it allows the upload of .abf and .dat files
2. it converts the .abf files into .csv files, and will perform baseline corrections and fast fourier transform.
3. it allows the choice of different models uploaded by the users
4. it allows the prediction of the data
5. it allows users to download the result as a .png file

### Built With

The backend authentication system was built with PHP based **Laravel 7** and **sanctum** was used to initiate a session token to the user.

The frontend portal was built with **Vue JS** and **Vuex** and **Vue Router** were used.

<!-- GETTING STARTED -->

## Getting Started

Please follow the instructions below for installation.

### Prerequisites

1. node.js
2. composer
3. Apache server with php 7.4.x
4. npm package manager
5. mySQL
6. git bash

### Installation

with the Apache server started and mySQL installed,

-   Backend:

    1. copy the environment: `cp .env.example .env`
    2. install all composer dependencies: `composer install`
    3. populate the user database: `php artisan migrate`

-   Frontend:

    1. npm install
    2. npm run admin-watch

-   production:

    1. npm run admin-prod
    2. in `resources/views/admin.blade.php`, change the `<script src="{{asset('admin/main.js')}}"></script>` to the hashed js file produced when production has been ran

<!-- Folder Structure -->

## Folder Structure

The structure is based on **Laravel 7** boiler plate `https://laravel.com/docs/7.x`

Below are some of the important directories that were used in the development.

├── **database/migrations** - this stores all of the SQL database and can be pushed to mySQL with the command `php artisan migrate:refresh`

├── **public/admin** - this stores all the `main.js` files and is responsible for the vue app as compiled by webpack

├── **resources/admin** - this stores all the files that is responsible for coding the web app. a more detailed explanation will be discussed in the next section

├── **routes** - `web.php` stores all the routes and can be modified based on the url of the website. `api.php` stores all the api and is responsible for the registration and login of the users.

├── **views** - `admin.blade.php` is equivalent to the `index.html` in the normal vue js boiler plate, but since laravel is PHP based, it will render the php file when the route is called.

└── **README.md**

## Code Explanation

### Introduction

This section will explain the files in the `/resources/admin` file.

### Page

-   only one page is used in this system, and is mainly comprised of the following components:

    1. upload files
    2. AbfList, ResultList, CsvList, ModelList

-   the Lists were stored in different arrays and was passed into various components as props
-   when the page is `mounted`in the vue lifecycle hook, it will run the four functions, getting data from the Python backend.

### Components

All the components can be found in side the `/components/home` file

#### AbfList, CsvList, ModelList, ResultList

-   the four components have the same format but will trigger different api functions according to functionality.

-   `AbfList.vue`: clicking `convert` button will trigger the `/file_conversion` api which prompts the server to convert the .abf to .csv file and perform the baseline correction operation.

-   `CsvList.vue`: clicking the `predict` button will trigger the `prediction/<file>/<model>` api and predict the converted csv and generate the result as an image

-   `ModelList.vue`: selecting the model will initiate an mutation to the `/store/data.js`. The use of Vuex is crucial since there will be communcation between two child components: ModelList and CsvList. The chosen model was then used in the subsequent step for prediction

-   `ResultList.vue`: clicking the `download` button will trigger the `get_image/<filename>` api and hence allow the user to download the result as an image from the server

#### UploadFile

-   component is reusable, allowing the upload of both the .abf and .dat files
-   it takes in a few properties:
    1. the title of the component
    2. the destination where the uploaded file should be stored
    3. the id of the input such that the `FormData()`can be sent to the server
