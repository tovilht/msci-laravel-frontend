<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Refill;

$factory->define(Refill::class, function (Faker $faker) {
    return [
        //
        'clinic_id' => $faker->numberBetween($min = 1, $max = 30),
        'done' => 0
    ];
});
