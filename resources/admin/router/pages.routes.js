export default [
    {
        path: "/auth/signin",
        name: "auth-signin",
        component: () =>
            import(
                /* webpackChunkName: "auth-signin" */ "@/pages/auth/SigninPage.vue"
            ),
        meta: {
            layout: "auth",
            loggedIn: true
        }
    },
    {
        path: "/auth/signup",
        name: "auth-signup",
        component: () =>
            import(
                /* webpackChunkName: "auth-signup" */ "@/pages/auth/SignupPage.vue"
            ),
        meta: {
            layout: "auth",
            loggedIn: true
        }
    },
    {
        path: "/error/not-found",
        name: "error-not-found",
        component: () =>
            import(
                /* webpackChunkName: "error-not-found" */ "@/pages/error/NotFoundPage.vue"
            ),
        meta: {
            layout: "error"
        }
    }
];
