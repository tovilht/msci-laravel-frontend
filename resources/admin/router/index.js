import Vue from "vue";
import Router from "vue-router";
import store from "../store/index";

// Routes
import PagesRoutes from "./pages.routes";
import HomePage from "../pages/home/HomePage";

Vue.use(Router);

export const routes = [
    ...PagesRoutes,

    {
        path: "/",
        name: "Home",
        component: HomePage,
        meta: { requiresAuth: true }
    },
    {
        path: "*",
        name: "error",
        component: () =>
            import(
                /* webpackChunkName: "error" */ "@/pages/error/NotFoundPage.vue"
            ),
        meta: {
            layout: "error"
        }
    }
];

const router = new Router({
    mode: "history",
    base: process.env.BASE_URL || "/",
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) return savedPosition;

        return { x: 0, y: 0 };
    },
    routes
});

/**
 * Before each route update
 */
// redirect pages that need authentication to login_page
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.state.auth.authenticate) {
            next({ path: "/auth/signin" });
            console.log("you are not logged in: redirect to signin page");
        } else {
            next();
        }
    } else {
        next();
    }

    // restrict user from entering into login or register page after logged in
    if (to.matched.some(record => record.meta.loggedIn)) {
        if (store.state.auth.authenticate) {
            next({ path: "/" });
            console.log("you are logged in: redirect to home_page");
        } else {
            next();
        }
    } else {
        next();
    }
});

/**
 * After each route update
 */
router.afterEach((to, from) => {});

export default router;
