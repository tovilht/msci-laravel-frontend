<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Inserting role types into role table
        DB::table('roles')->insert([
            ['name' => 'customer'],
            ['name' => 'clinic'],
            ['name' => 'sale']
        ]);

        // Distrcts and its name
        DB::table('districts')->insert([
            // Region 1: Hong Kong Island (1-4)
            ['region' => '1', 'region_name' => 'Hong Kong Island', 'name' => 'Central and Western'],
            ['region' => '1', 'region_name' => 'Hong Kong Island', 'name' => 'Eastern'],
            ['region' => '1', 'region_name' => 'Hong Kong Island', 'name' => 'Southern'],
            ['region' => '1', 'region_name' => 'Hong Kong Island', 'name' => 'Wan Chai'],

            // Region 2: Kowloon (5-9)
            ['region' => '2', 'region_name' => 'Kowloon', 'name' => 'Sham Shui Po'],
            ['region' => '2', 'region_name' => 'Kowloon', 'name' => 'Kowloon City'],
            ['region' => '2', 'region_name' => 'Kowloon', 'name' => 'Kwun Tong'],
            ['region' => '2', 'region_name' => 'Kowloon', 'name' => 'Wong Tai Sin'],
            ['region' => '2', 'region_name' => 'Kowloon', 'name' => 'Yau Tsim Mong'],

            // Region 3: New Territories (10-18)
            ['region' => '3', 'region_name' => 'New Territories', 'name' => 'Islands'],
            ['region' => '3', 'region_name' => 'New Territories', 'name' => 'Kwai Tsing'],
            ['region' => '3', 'region_name' => 'New Territories', 'name' => 'North'],
            ['region' => '3', 'region_name' => 'New Territories', 'name' => 'Sai Kung'],
            ['region' => '3', 'region_name' => 'New Territories', 'name' => 'Sha Tin'],
            ['region' => '3', 'region_name' => 'New Territories', 'name' => 'Tai Po'],
            ['region' => '3', 'region_name' => 'New Territories', 'name' => 'Tsuen Wan'],
            ['region' => '3', 'region_name' => 'New Territories', 'name' => 'Tuen Mun'],
            ['region' => '3', 'region_name' => 'New Territories', 'name' => 'Yuen Long'],

            // Region 4: Marine (19)
            ['region' => '4', 'region_name' => 'Marine', 'name' => 'Marine'],
        ]);

        //     // creating first batch of new products with new records
        //     factory(App\Product::class, 20)->create()->each(function ($product) {
        //         unset($product->ordering);
        //         $record = App\Record::create($product->toArray());
        //         $record->product()->associate($product);
        //         $record->save();
        //     });

        //     // updating the second batch of products with new records
        //     $products = App\Product::all();
        //     foreach ($products as $product) {
        //         $product->name = "new " . $product->name;
        //         $product->price = $product->price * 100;
        //         $product->clinic_price = $product->clinic_price * 100;
        //         $product->update($product->toArray());
        //         $id = $product->id;
        //         unset($product->id);
        //         unset($product->ordering);
        //         $record = App\Record::create($product->toArray());
        //         $product = App\Product::find($id);
        //         $record->product()->associate($product);
        //         $record->save();
        //     }

        //     factory(App\Order::class, 30)->create();

        //     factory(App\Refill::class, 30)->create();

        //     factory(App\User::class, 100)->create()->each(function ($user) {
        //         if ($user->role->id == 1) {
        //             $user->customer()->createMany(factory(App\Customer::class, 1)->make()->toArray());
        //         } elseif ($user->role->id == 2) {
        //             $user->clinic()->createMany(factory(App\Clinic::class, 1)->make()->toArray());
        //         } elseif ($user->role->id == 3) {
        //             $user->sale()->createMany(factory(App\Sale::class, 1)->make()->toArray());
        //         }
        //     });

        //     // many to many relationship seeding
        //     $clinics = App\Clinic::all();
        //     $products = App\Product::all();
        //     $orders = App\Order::all();
        //     $records = App\Record::all();
        //     $refills = App\Refill::all();


        //     $clinics->each(function ($clinic) use ($products) {
        //         for ($i = 1; $i < 21; $i++) {
        //             $clinic->products()->attach($i);
        //         }
        //     });

        //     $orders->each(function ($order) use ($records) {
        //         $order->records()->attach(
        //             $records->random(rand(1, 10))->pluck('id')->toArray()
        //         );
        //     });

        //     $refills->each(function ($refills) use ($records) {
        //         $refills->records()->attach(
        //             $records->random(rand(1, 10))->pluck('id')->toArray()
        //         );
        //     });
    }
}
