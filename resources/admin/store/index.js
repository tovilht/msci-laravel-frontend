import Vue from "vue";
import Vuex from "vuex";

// Global vuex
import AppModule from "./app";
import Auth from "./auth";
import Data from "./data";

Vue.use(Vuex);

/**
 * Main Vuex Store
 */
const store = new Vuex.Store({
    modules: {
        app: AppModule,
        auth: Auth,
        data: Data
    }
});

export default store;
