import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import axios from "axios";

// PLUGINS
import vuetify from "./plugins/vuetify";
import i18n from "./plugins/vue-i18n";
import "./plugins/vue-google-maps";
import "./plugins/vue-shortkey";
import "./plugins/vue-head";
import "./plugins/vue-gtag";
import "./plugins/apexcharts";
import "./plugins/echarts";
import "./plugins/animate";
import "./plugins/clipboard";
import "./plugins/moment";

// FILTERS
import "./filters/capitalize";
import "./filters/lowercase";
import "./filters/uppercase";
import "./filters/formatCurrency";
import "./filters/formatDate";

// STYLES
// Main Theme SCSS
import "./assets/scss/theme.scss";

// Animation library - https://animate.style/
import "animate.css/animate.min.css";

// Set this to false to prevent the production tip on Vue startup.
Vue.config.productionTip = false;

// AXIOS GLOBAL SETTINGS
// for Python server
Vue.prototype.$pyAxios = axios.create({
    baseURL: "http://127.0.0.1:5000/"
});
// for PHP server
Vue.prototype.$phpAxios = axios.create({
    baseURL: "/api/"
});

store.dispatch("check_logged_in").then(() => {
    new Vue({
        i18n,
        vuetify,
        router,
        store,
        render: h => h(App)
    }).$mount("#app");
});
