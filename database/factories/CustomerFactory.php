<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Customer;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        //
        'created_at' => $faker->dateTimeThisMonth($max = 'now', $timezone = null),
    ];
});
