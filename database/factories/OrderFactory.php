<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Order;

$factory->define(Order::class, function (Faker $faker) {
    return [
        //
        'order_type' => 0,
        'clinic_id' => $faker->numberBetween($min = 1, $max = 30),
        'customer_id' => $faker->numberBetween($min = 1, $max = 30),
        'paid' => 0,
        'created_at' => $faker->dateTimeThisMonth($max = 'now', $timezone = null),
        'updated_at' => $faker->dateTimeThisMonth($max = 'now', $timezone = null)
    ];
});
